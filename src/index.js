import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import EditStudent from "./Components/crud/EditStudent";
import ListStudents from "./Components/ListStudents";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <EditStudent />
    <ListStudents />
  </React.StrictMode>
);

reportWebVitals();

import React, { useEffect, useState } from 'react';
import { ref, get, update } from "firebase/database";
import StartFirebase from "../firebaseConf/index";
import './EditStudent.css';

function EditStudent() {
    const [student, setStudent] = useState({
        studentid: '',
        name: '',
        city: '',
        email: '',
        phone: ''
    });

    const db = StartFirebase();

    useEffect(() => {
        const studentRef = ref(db, `students/${student.studentid}`);
        get(studentRef).then((snapshot) => {
            if (snapshot.exists()) {
                setStudent(snapshot.val());
            } else {
                console.log("No data available");
            }
        }).catch((error) => {
            console.error(error);
        });
    }, [db, student.studentid]);

    const handleInputChange = (event) => {
        setStudent({
            ...student,
            [event.target.name]: event.target.value
        });
    };

    const save = () => {
        const updateRef = ref(db, `students/${student.studentid}`);
        update(updateRef, student)
            .then(() => alert("Data updated successfully!"))
            .catch((error) => alert("Failed to update data: " + error.message));
    };

    return (
        <div className="edit-student-container">
            <label htmlFor="studentid" className="label">Student ID:</label>
            <input id="studentid" name="studentid" className="input" value={student.studentid} onChange={handleInputChange} />

            <label htmlFor="name" className="label">Name:</label>
            <input id="name" name="name" className="input" value={student.name} onChange={handleInputChange} />

            <label htmlFor="city" className="label">City:</label>
            <input id="city" name="city" className="input" value={student.city} onChange={handleInputChange} />

            <label htmlFor="email" className="label">Email:</label>
            <input type="email" id="email" name="email" className="input" value={student.email} onChange={handleInputChange} />

            <label htmlFor="phone" className="label">Phone:</label>
            <input type="tel" id="phone" name="phone" className="input" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" value={student.phone} onChange={handleInputChange} />

            <button className="button" onClick={save}>Save</button>
        </div>
    );
}

export default EditStudent;

import React, { useEffect, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { ref, get } from "firebase/database";
import StartFirebase from "./firebaseConf/index";

const ListStudents = () => {
    const [students, setStudents] = useState([]);

    useEffect(() => {
        const db = StartFirebase();
        const dbRef = ref(db, 'students');

        get(dbRef).then((snapshot) => {
            if (snapshot.exists()) {
                const data = snapshot.val();
                const studentsArray = Object.keys(data).map(key => ({
                    ...data[key],
                    id: key
                }));
                setStudents(studentsArray);
            } else {
                console.log("No data available");
            }
        }).catch((error) => {
            console.error(error);
        });
    }, []);

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Student ID</TableCell>
                        <TableCell align="right">Name</TableCell>
                        <TableCell align="right">City</TableCell>
                        <TableCell align="right">Email</TableCell>
                        <TableCell align="right">Phone</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {students.map((student) => (
                        <TableRow key={student.id}>
                            <TableCell component="th" scope="row">
                                {student.studentid}
                            </TableCell>
                            <TableCell align="right">{student.name}</TableCell>
                            <TableCell align="right">{student.city}</TableCell>
                            <TableCell align="right">{student.email}</TableCell>
                            <TableCell align="right">{student.phone}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default ListStudents;
